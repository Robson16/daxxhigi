<?php

/**
 * Functions to enhance and add features to the Woocommerce
 */

function higimulher_woocommerce_scripts()
{
    if (is_product()) {
        wp_enqueue_style('owlcarousel', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css', null, '2.3.4', 'all');
        wp_enqueue_style('owltheme', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css', null, '2.3.4', 'all');
        wp_enqueue_script('owlcarousel', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array('jquery'), '2.3.4', true);
        wp_enqueue_script('woo-related-products', get_template_directory_uri() . '/assets/js/frontend/woo-related-products.js', array('jquery'), wp_get_theme()->get('Version'), true);
    }
}
add_action('wp_enqueue_scripts', 'higimulher_woocommerce_scripts');

// Add a button link to product single on product list item (content-product.php)
function add_single_product_details_button()
{
    global $product;

    $link = $product->get_permalink();
    $name = __("Details", "higimulher");
    $class = 'button product-details';

    echo '<a href="' . $link . '" class="' . $class . '">' . $name . '</a>';
}
add_action('woocommerce_after_shop_loop_item', 'add_single_product_details_button', 5);

// Remove the breadcrumb from product single
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

// Remove the sidebar from product single
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

// Remove the default WooCommerce external Buy Product button on individual Product page.
remove_action('woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30);

// This will take care of the Buy Product button below the external product on the Shop page.
function higimulher_external_add_product_link($link)
{
    global $product;

    if ($product->is_type('external')) {

        $link = sprintf(
            '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" target="_blank">%s</a>',
            esc_url($product->add_to_cart_url()),
            esc_attr(isset($quantity) ? $quantity : 1),
            esc_attr($product->id),
            esc_attr($product->get_sku()),
            esc_attr(isset($class) ? $class : 'button product_type_external'),
            esc_html($product->add_to_cart_text())
        );
    }

    return $link;
}
add_filter('woocommerce_loop_add_to_cart_link', 'higimulher_external_add_product_link', 10, 2);

// Add the open in a new browser tab WooCommerce external Buy Product button.
function higimulher_external_add_to_cart()
{
    global $product;

    if (!$product->add_to_cart_url()) {
        return;
    }

    $product_url = $product->add_to_cart_url();
    $button_text = $product->single_add_to_cart_text();

    /**
     * The code below outputs the edited button with target="_blank" added to the html markup.
     */
    do_action('woocommerce_before_add_to_cart_button'); ?>

    <p class="cart">
        <a href="<?php echo esc_url($product_url); ?>" rel="nofollow" class="single_add_to_cart_button button alt" target="_blank">
            <?php echo esc_html($button_text); ?>
        </a>
    </p>

<?php do_action('woocommerce_after_add_to_cart_button');
}
add_action('woocommerce_external_add_to_cart', 'higimulher_external_add_to_cart', 30);

/**
 * Change number of related products output
 * @see https://docs.woocommerce.com/document/change-number-of-related-products-output/
 */
function higimulher_related_products_args($args)
{
    $args['posts_per_page'] = 12; // related products
    return $args;
}
add_filter('woocommerce_output_related_products_args', 'higimulher_related_products_args', 20);
