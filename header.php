<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<header id="header">
		<nav id="navbar" class="navbar navbar-expand-lg navbar-light">
			<div class="container">
				<?php if (function_exists('the_custom_logo') && has_custom_logo()) : ?>
					<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
						<img class="img-fluid" src="<?php echo wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0]; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
					</a>
				<?php else : ?>
					<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
						<h1><?php echo get_bloginfo('title'); ?></h1>
					</a>
				<?php endif; ?>

				<?php get_template_part('partials/social/social', 'networks-icons'); ?>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<?php
				wp_nav_menu(array(
					'theme_location' => 'main_menu',
					'depth' => 2,
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'main-navbar',
					'menu_class' => 'navbar-nav mr-auto',
					'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
					'walker' => new WP_Bootstrap_Navwalker()
				));
				?>
			</div>
		</nav>
	</header>
