<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

<?php if (get_theme_mod('setting_whatsapp')) : ?>
	<a id="whatapp-cta" class="whatapp-cta" href="<?php echo esc_url(get_theme_mod('setting_whatsapp')); ?>" target="_blank" rel="noopener">
		<i class="fab fa-whatsapp fa-2x"></i>
		<span>
			<?php echo _e('Click here and contact us by Whatsapp', 'higimulher'); ?>
		</span>
	</a>
<?php endif; ?>

<footer id="footer" class="footer">
	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('higimulher-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('higimulher-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('higimulher-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('higimulher-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="footer-copyright">
		<div class="container">
			<span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php _e('All rights reserved.', 'higimulher') ?></span>
			<span>
				<?php _e('Developed by:', 'higimulher') ?>
				&nbsp;
				<a href="https://www.vollup.com/" target="_blank">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/vollup-brand-white.png' ?>" alt="Vollup">
				</a>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->

</footer>

<?php wp_footer(); ?>

</body>

</html>
