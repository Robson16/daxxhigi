<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<div class="product-summary-wrapper">
		<div class="container">
			<div class="product-summary">
				<?php
				/**
				 * Hook: woocommerce_before_single_product_summary.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
				remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
				do_action( 'woocommerce_before_single_product_summary' );
				?>

				<div class="product-image">
					<?php the_post_thumbnail( 'large' ); ?>
				</div>

				<div class="summary entry-summary">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40); // Remove SKU, categorys, etc...
					add_action( 'woocommerce_single_product_summary', 'woocommerce_output_all_notices', 40 );
					do_action( 'woocommerce_single_product_summary' );
					?>
				</div>
				<!-- /.summary -->
			</div>
			<!-- /.product-summary -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.product-summary-wrapper -->

	<div class="container">

		<?php
		/**
		 * OBS: Posteriormente organizar em uma funcão colocando no hook abaixo 
		 */ 
		if ( get_the_content() ) : 
		?>
			<div class="product-content">
				<?php the_content(); ?>

				<p class="cart" style="margin-top: 4rem; font-size: 1rem; text-align: center; text-transform: uppercase;">
					<a href="<?php echo $product->add_to_cart_url() ?>" rel="nofollow" class="single_add_to_cart_button button alt" target="_blank">
						<?php echo $product->add_to_cart_text() ?>       
					</a>
				</p>
			</div>
		<?php endif; ?>

		<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
		do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div>
	<!-- /.container -->
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
