<?php

/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if ($upsells) : ?>

	<section class="up-sells upsells products">
		<?php
		$heading = apply_filters('woocommerce_product_upsells_products_heading', __('See too', 'higimulher'));

		if ($heading) :
		?>
			<h2 class="section-title"><?php echo esc_html($heading); ?></h2>
		<?php endif; ?>

		<div class="owl-carousel owl-theme">

			<?php foreach ($upsells as $upsell) : ?>

				<?php
				$post_object = get_post($upsell->get_id());

				setup_postdata($GLOBALS['post'] = &$post_object); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found
				?>

				<a class="product-link" href="<?php echo $upsell->get_permalink(); ?>" target="_self">
					<?php
					the_post_thumbnail(
						'shop_catalog',
						array(
							'class' => 'product-image',
							'alt' => get_the_title(),
						)
					);
					the_title('<h3 class="product-title">', '</h3>');
					?>
				</a>

			<?php endforeach; ?>

		</div>
		<!-- /.owl-carousel -->

	</section>

<?php
endif;

wp_reset_postdata();
