<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
	<header style="background-image: url(<?php echo get_header_image() ?>); ">
		<div class="container">
			<?php
			if (is_search()) echo sprintf('<h1>%s <span>%s</span></h1>', __('Search results for:', 'higimulher'), get_search_query());
			if (is_archive()) echo sprintf('<h1>%s</h1>', get_the_archive_title());
			if (is_home()) echo '<h1>Blog</h1>';
			?>
		</div>
	</header>
	<div class="container">
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				get_template_part('partials/content/content', 'excerpt');
			}

			the_posts_pagination();
		} else {
			get_template_part('partials/content/content', 'none');
		}
		?>
	</div>
	<!--/.container-->
</main>

<?php
get_footer();
