export default class WhatsCTAStickyFooter {
    constructor() {
        this.footer = document.querySelector("#footer");

        this.whatsCta = document.querySelector("#whatapp-cta");

        this.viwewportY = window.innerHeight;

        this.events();
    }

    // Events
    events() {
        this.handleCTASticky();
    }

    // Methods
    handleCTASticky() {
        window.addEventListener("scroll", () => {
            if (window.scrollY + window.innerHeight - this.footer.offsetHeight > this.footer.offsetTop) {
                this.whatsCta.style.marginBottom = this.footer.offsetHeight + "px";
            } else {
                this.whatsCta.style.marginBottom = 'unset';
            }
        });
    }
}